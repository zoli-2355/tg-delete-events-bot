FROM golang:1.13-alpine3.10 AS build
# Support CGO and SSL
RUN apk --no-cache add gcc g++ make
RUN apk add git

RUN mkdir -p /go/src/app
WORKDIR /go/src/app
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=1 GOOS=linux go build -ldflags="-s -w" -o ./bin/tg-delete-events-bot ./main.go

FROM alpine:3.10
RUN apk --no-cache add ca-certificates openssl
WORKDIR /usr/bin
COPY --from=build /go/src/app/bin/tg-delete-events-bot /bin/tg-delete-events-bot
COPY docker-entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD []