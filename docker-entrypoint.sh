#!/bin/sh

export TLS_CERT_PATH=/cert.pem
export TLS_KEY_PATH=/key.pem
openssl req -x509 -newkey rsa:2048 -keyout /key.pem -out /cert.pem -days 3560 -subj "/O=Org/CN=tgbot.frustrat.es" -nodes
chmod 600 /key.pem
export SECURE_RANDOM_PATH=$(openssl rand -base64 32)

exec /bin/tg-delete-events-bot