package main

import (
	"log"
	"os"

	tb "gopkg.in/tucnak/telebot.v2"
)

func main() {
	botToken := os.Getenv("TELEGRAM_BOT_TOKEN")
	if botToken == "" {
		log.Fatal("missing TELEGRAM_BOT_TOKEN")
	}
	tlsCertPath := os.Getenv("TLS_CERT_PATH")
	if tlsCertPath == "" {
		tlsCertPath = "./tls/cert.pem"
	}
	tlsKeyPath := os.Getenv("TLS_KEY_PATH")
	if tlsKeyPath == "" {
		tlsKeyPath = "./tls/key.pem"
	}
	secureRandomPath := os.Getenv("SECURE_RANDOM_PATH")
	if secureRandomPath == "" {
		log.Fatal("missing SECURE_RANDOM_PATH")
	}

	webhook := &tb.Webhook{
		Listen: "0.0.0.0:8443",
		TLS: &tb.WebhookTLS{
			Key:  tlsKeyPath,
			Cert: tlsCertPath,
		},
		Endpoint: &tb.WebhookEndpoint{
			PublicURL: "https://tgbot.frustrat.es:8443/" + secureRandomPath,
			Cert:      tlsCertPath,
		},
	}
	bot, err := tb.NewBot(tb.Settings{
		Token: botToken,
		// Verbose: true,
		Poller: webhook,
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	// x, e := bot.GetWebhook()
	// log.Printf("%v,%v", x, e)
	// bot.Handle(tb.OnText, func(msg *tb.Message) {
	// 	log.Printf("%d", time.Now().Unix())
	// 	log.Printf("%d", msg.Unixtime)
	// })

	bot.Handle(tb.OnUserJoined, func(msg *tb.Message) {
		if msg.UserJoined.IsBot {
			member, err := bot.ChatMemberOf(msg.Chat, msg.UserJoined)
			if err != nil {
				log.Printf("could not get ChatMember %d: %v", msg.Sender.ID, err)
			}
			bot.Ban(msg.Chat, member)
		}
		bot.Delete(msg)
	})
	bot.Handle(tb.OnUserLeft, func(msg *tb.Message) {
		bot.Delete(msg)
	})
	bot.Start()
}
